$( document ).ready(function() {
    console.log("Página cargada");

    lista = [];
    listaDesordenada = [];
    var tope = 100000;
    for(var i=0; i<tope; i++){
        lista.push(i);
        listaDesordenada.push(Math.round(tope * Math.random()));
    }
    var searchValue = lista[Math.floor(tope * Math.random())];
    console.log(searchValue);

    /*
    búsqueda de elementos secuencial
     */
    var now = Date.now();
    for(var i=0; i<lista.length; i++){
        var newVal = lista[i];
        if(newVal == searchValue){
            console.log("Encontrado!");
            break;
        }
    }
    var time = Date.now() - now;
    console.log("Ha necesitado " + time);


    /*
    búsqueda de elementos binaria
     */
    var now = Date.now();
    search = function(lista, searchValue){
        var low = 0;
        var high = lista.length - 1;

        while(low <= high) {
            var middle = Math.floor((low + high)/2);
            var guess = lista[middle];
            if(guess == searchValue){
                console.log("Encontrado!");
                return middle;
            }
            if(guess > searchValue){
                high = middle - 1;
            } else {
                low = middle + 1;
            }
        }
        return -1;
    }
    search(lista, searchValue);
    var time = Date.now() - now;
    console.log("Ha necesitado " + time);


    /*
    ordenación secuencial: bubble sort
     */
    var now = Date.now();
    bubbleSort = function(lista){
        var temp = 0;
        for(var i=0; i<lista.length; i++){
            for(var j=0; j<i; j++){
                if(lista[j] > lista[j+1]){
                    temp = lista[j];
                    lista[j] = lista[j + 1];
                    lista[j + 1] = temp;
                }
            }
        }
        return lista;
    }
    var newLista = bubbleSort(listaDesordenada.slice());
    var time = Date.now() - now;
    console.log("Ha necesitado " + time);


    /*
    ordenación binaria
     */
    var now = Date.now();
    quickSort = function(lista){
        if(lista.length < 2){
            return lista;
        } else {
            var left = [];
            var right = [];
            var newArray = [];
            var pivot = lista.pop();
            var length = lista.length;

            for (var i = 0; i < length; i++) {
                if (lista[i] <= pivot) {
                    left.push(lista[i]);
                } else {
                    right.push(lista[i]);
                }
            }

            return newArray.concat(quickSort(left), pivot, quickSort(right));
        }
    }
    var newLista = quickSort(listaDesordenada.slice());
    var time = Date.now() - now;
    console.log("Ha necesitado " + time);
});

