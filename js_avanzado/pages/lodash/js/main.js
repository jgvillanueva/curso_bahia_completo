$( document ).ready(function() {
    init();
});

var objList = {};
// función invocable sólamente una vez
init = _.once(_init);
function _init(){
    console.log("Página cargada");

    data =  JSON.parse('[{"id":"1","bird_name":"Mirlo","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_01.png","bird_sightings":"435","mine":0},{"id":"2","bird_name":"Estornino común","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_02.png","bird_sightings":"195","mine":0},{"id":"3","bird_name":"Garceta comúnn","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_02.png","bird_sightings":"79","mine":0},{"id":"7","bird_name":"Chorlito","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_02.png","bird_sightings":"75","mine":0},{"id":"430","bird_name":"Mirlo","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_05.png","bird_sightings":"42","mine":0},{"id":"431","bird_name":"Estornino común","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_02.png","bird_sightings":"25","mine":0},{"id":"432","bird_name":"Estornino común","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_04.png","bird_sightings":"25","mine":0},{"id":"433","bird_name":"Estornino común","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_02.png","bird_sightings":"15","mine":0},{"id":"434","bird_name":"Estornino comn","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_03.png","bird_sightings":"13","mine":0},{"id":"435","bird_name":"Estornino comn","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_03.png","bird_sightings":"12","mine":0},{"id":"436","bird_name":"Estornino comn","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_04.png","bird_sightings":"11","mine":0},{"id":"437","bird_name":"Estornino comn","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_02.png","bird_sightings":"13","mine":0},{"id":"438","bird_name":"Estornino comn","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_02.png","bird_sightings":"16","mine":0},{"id":"439","bird_name":"Mirlo","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_04.png","bird_sightings":"24","mine":0},{"id":"440","bird_name":"Mirlo","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_03.png","bird_sightings":"42","mine":0},{"id":"390","bird_name":"Pajarillo Rillo","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_02.png","bird_sightings":"2","mine":1},{"id":"601","bird_name":"p`\u00e7\u00e7p`\u00e7","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_03.png","bird_sightings":"1","mine":1},{"id":"602","bird_name":"p`\u00e7\u00e7p`\u00e7","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_05.png","bird_sightings":"1","mine":1},{"id":"603","bird_name":"p`\u00e7\u00e7p`\u00e7","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_03.png","bird_sightings":"1","mine":1},{"id":"666","bird_name":"Eererer","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_05.png","bird_sightings":"2","mine":1},{"id":"671","bird_name":"sdsdsds","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_04.png","bird_sightings":"2","mine":1},{"id":"753","bird_name":"aaa","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_01.png","bird_sightings":"7","mine":1},{"id":"948","bird_name":"Perico","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_04.png","bird_sightings":"3","mine":1},{"id":"1192","bird_name":"Paloma","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_04.png","bird_sightings":"0","mine":1},{"id":"1317","bird_name":"ga","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_02.png","bird_sightings":"0","mine":1},{"id":"1320","bird_name":"hds","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_04.png","bird_sightings":"0","mine":1},{"id":"1321","bird_name":"shd","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_02.png","bird_sightings":"0","mine":1},{"id":"1322","bird_name":"fdsa","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_05.png","bird_sightings":"0","mine":1},{"id":"1325","bird_name":"Mirlo","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_05.png","bird_sightings":"0","mine":1},{"id":"1406","bird_name":"Mirlo","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_05.png","bird_sightings":"0","mine":1},{"id":"1623","bird_name":"Pocha comn","bird_image":"http://dev.contanimacion.com/birds/public/assets/bird_04.png","bird_sightings":"0","mine":1}]');


    /*
    Valores
     */
    // valor random
    $content = $('.content');
    $content.caja_plugin({text: _.random(2,3)})
    // repetir función varias veces
    _.times(4, () => {
        $content.caja_plugin({text: _.random(1,5)})
    })
    // comparación de valores
    var lista = [1, "wer", 2.34, Math.PI, "fdfsfsd", 'sddsfgg', function(){}, {data:1} ];
    for(var i=0; i<lista.length; i++){
        $content.caja_plugin({text: getType(lista[i])})
    }
    function getType(val){
        if (_.isNumber(val)) {
            return (val + 'is a number');
        }
        if (_.isString(val)) {
            return (val + ' is a string');
        }
        if (_.isArray(val)) {
            return(val + ' is an array');
        }
        if (_.isObject(val)) {
            return (val + ' is an object');
        }
    }

    //máximo y mìnimo
    lista = [-3, 4, 0, 12, 43, 9, -12, 234, -12];
    $content.caja_plugin({text: "el máximo es: " + _.max(lista)});
    $content.caja_plugin({text: "el mínimo es: " + _.min(lista)});

    var coleccion = [];
    $('#b_add').click(function(){
        valor = $('#nombre').val();
        console.log($('#nombre').val());
        if(_.isNil(valor) || valor === ''){
            alert("Introduce un valor");
        }else{
            // partiendo en palabras un texto
            var palabras = _.words(valor);
            if(palabras.length > 1){
                var nombres = [];
                _.each(palabras, function(item){
                    nombres.push({
                        nombre:item,
                        id:_.uniqueId('el_')
                    });
                });
                coleccion = _.union(coleccion, nombres);
            }else{
                coleccion.push({nombre: valor, id:_.uniqueId('el_')})
            }
            reload(coleccion);
        }
        objList = _.keyBy(coleccion, 'id');
    });


    // contar sobre el uso de change
    $('#filter').keypress(function(){
        var valor = $(this).val();
        var filtered = _.filter(coleccion, function(item){
            return item.nombre.indexOf(valor) >= 0;
        });
        reload(filtered);
    })

    // invocación sólo cada cierto tiempo para evitar repeticiones y atascos
    jQuery(window).on('scroll', _.throttle(updatePosition, 1000));
    function updatePosition(){
        // aquí podemos añadir el tema de hacer el header más pequeño
        console.log(window.scrollY);
        if(window.scrollY > 55){
            $('body > .navbar').addClass('contraido');
        } else{
            $('body > .navbar').removeClass('contraido');
        }
    }
}

function reload(coleccion){
    $ul = $('<ul></ul>');
    // aprovechar para hablar de map:
    /*
    coleccion.map(function(item){
        return(item);
    )
     */
    // iteraciones sobre objetos
    _.each(coleccion, function(item){
        $li = $('<li></li>')
            .text(item.nombre)
            .attr('data-id', item.id);
        $btn_view = $('<button class="btn">View</button>')
            .on('click', function(){
                showElement(coleccion, item.id);
            })
        $btn_del = $('<button class="btn">Delete</button>')
            .on('click', function(){
                deleteItem(coleccion, item.id);
            })
        $li.append($btn_view);
        $li.append($btn_del);
        $ul.append($li);
    })
    $('#tabla').html($ul);
}

// remove elements de una lista
function deleteItem(coleccion, id){
    // eliminando un elemento de una colección
   _.remove(coleccion, function(item) {
        return item.id === id;
    });
    reload(coleccion);
}

function showElement(coleccion, id){
    // find sobre colecciones
    var item = _.find(coleccion, function(item) { return item.id == id; });
    $('<div></div>')
        .html('<h1>'+item.nombre+'</h1>')
        .modal();
    console.log(objList[id]);
}

(function($){
    $.fn.caja_plugin = function(options){
        var settings = $.extend({
            text: "Caja vacía",
        }, options );
        console.log(settings.text);

        $p = $('<p></p>')
            .text(settings.text)
            .addClass('caja');
        this.append($p);

        return this;
    }
})($)
