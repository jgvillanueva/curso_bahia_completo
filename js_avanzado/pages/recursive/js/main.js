$( document ).ready(function() {
    console.log("Página cargada");

    lista = [1,2,3,4,5,6,7,8];

    // bucle normal
    var resultado = 1;
    for(var i=0; i< lista.length; i++){
        console.log(resultado);
        resultado = resultado*lista[i];
    }
    console.log(resultado);

    // bucle recursivo
    resultado = 1;
    var loop = function(index, lista, callBack){
        if(index >= lista.length){
            return
        }
        console.log(resultado);
        resultado = callBack(resultado, lista[index]);
        loop (index += 1, lista, callBack)
    }

    loop(0, lista, function(val1, val2){
        return val1 * val2;
    });
    console.log(resultado);

    // recursividad aplicada a modificar el DOM
    marcaEtiquetas = function(element){
        if(element.tagName != 'script'){
            addName(element);
            for(var i=0; i<element.childNodes.length; i++){
                marcaEtiquetas(element.childNodes[i]);
            }
        }
        function addName(element){
            element.innerHTML = element.tagName + ": " + element.innerHTML;
        }
    }
    marcaEtiquetas(document.body);
});
