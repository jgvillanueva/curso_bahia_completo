$( document ).ready(function() {
    console.log("Página cargada");

    var slash = /\//;
    var texto

    /*
    búsquedas
     */
    // recupera la posición
    console.log("AC/DC".search(slash)); //2

    // comprueba principio ^
    console.log('inicio');
    console.log("AwerwerAsdf".search(/^A/)); // 0
    console.log("asdasdAwerwerAsdf".search(/^A/)); // -1

    // comprueba final $
    console.log('final');
    console.log("weerwerAsdfA".search(/A$/)); // 0
    console.log("asdasdAwerwerAsdf".search(/A$/)); // -1

    // comprueba números \d (número) y \D (no número)
    // lo mismo para caracteres alfanuméricos se hace con \w y \W
    console.log('un número');
    console.log("weerw4erAsdfA".search(/\d/)); // 0
    console.log("asdasdAwerwerAsdf".search(/\d/)); // -1
    console.log("2342a3424234".search(/\D/)); // 4
    console.log("3243422".search(/\D/)); // -1

    // comprobar rangos
    console.log('rangos de caracteres');
    console.log('qqwqwqwqwdqwqww'.search(/[a-e]/)); // 9

    // comprobar contraseñas:
    reg = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/
    console.log("contraseña, mínimo 8, un número, caracteres especiales y una letra mayúscula");
    console.log('longitud de número', '65443234'.search(/^\d{8}$/)); //0


    /*
    [sdr] uno de esos
    [^sdr] ^negación: ninguno de esos
    + busca uno o más veces
    ? puede o no. Así das la opción de que haya algo por medio
    a{3} comprueba que el precedente a esté 3 veces
    a{1,3} entre 1 y 3 veces
    x|y busca x o y

     */

    /*
     replacements
      */
    // replace de un caracter
    texto = 'When the first argument of replace is a string, it only looks for the first match.\n' +
        '\n' +
        'To find all dashes, we need to use not the string';
    console.log(texto.replace(/\n/g, " "));

    // replace de un caracter seguido
    // de cualquier otro variable y luego un último
    texto = "saco seco sico soco suco";
    console.log(texto.replace(/s.c/g, "_"));

    // añadir contenidos cuando encuentre elementos
    //$ añade lo sisguiente variable
    //& añade el resto del contenido
    texto = "John Doe, John Smith and John Bull.";
    console.log(texto.replace(/John/g, 'Mr. $&'));
});
