$( document ).ready(function() {
    console.log("Página cargada");

    function render(value){
        $("#result_container").append( '<p>'+value+'</p>' );
    }

    var poligono1 = new Poligono(10, 20);
    var poligono2 = new Poligono(20, 30);
    render("El área del polígono 1 es: " + poligono1.getArea());
    render("El área del polígono 2 es: " + poligono2.getArea());
    // transferencia de contextos usando call
    var getArea = poligono1.getArea;
    render("El área del polígono 2 es: " + getArea.call(poligono2));
    // transferencia de contextos usando bind
    var getArea2 = poligono1.getArea.bind(poligono2);
    render("El área del polígono 2 es: " + getArea2());

    // herencia
    var caja = new Caja(80, 120, 2);
    caja.render();
    //compruebo que realmente hereda las dos clases
    console.log(caja instanceof Poligono);
    console.log(caja instanceof Caja);
});


function Poligono(_alto, _ancho){
    this.alto = _alto;
    this.ancho = _ancho;
}
Poligono.prototype.getArea = function(){
    return this.alto * this.ancho;
}

// crea nueva clase
function Caja(_alto, _ancho, _borde){
    //pasa los parámetros compartidos al constructor
    Poligono.call(this, _alto, _ancho);

    this.borde = _borde;
}
// envía el contexto this a su parent, para poder usar sus métodos
Caja.prototype = Object.create(Poligono.prototype)
Caja.prototype.render = function(){
    var $div = $('<div class="caja">'+this.getArea()+'</div>');
    $div.css({borderWidth: this.border + 'px',
        width: this.width,
        height: this.height
    })
    $("#result_container")
        .append($div);
}
Caja.prototype.getArea = function(){
    return 'Área: ' + this.alto * this.ancho;
}

