(function logger(window){
    window.logger = this;
    this.log = function(text){
        var now = new Date();
        var nowText = now.getHours() + "-" + now.getMinutes()+ "-" + now.getSeconds()
        console.log("Logger::" + nowText + ": " + text);
    }
})(window)