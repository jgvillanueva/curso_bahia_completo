$( document ).ready(function() {
    console.log("Página cargada");

    $.getScript("js/logger.js")
        .done(function(script, textStatus){
            logger.log("Script cargado correctamente");
        })
        .fail(function(jqxhr, settings, exception){
            console.log("Fallo cargando script", exception);
        })
});
