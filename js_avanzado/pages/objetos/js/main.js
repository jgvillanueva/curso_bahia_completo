$( document ).ready(function() {
    console.log("Página cargada");

    /*
    definición de objetos
     */
    function render(value){
        $("#result_container").append( '<p>'+value+'</p>' );
    }

    /*
    POO básica
    expresión de objetos: molesto, poco ordenado, sin control
    se usa || para asegurar que no se repite su creación
     */
    var myObject = myObject || {
        value: 'objeto',
        getValue: function(){
            return this.value;
        }
    }
    render(myObject.getValue());

    /*
    Uso de subespacios de nombres
    Recordar la pérdida de conexto al usar espacios de nombres
     */
    var objetoPadre = objetoPadre || {
        valuePadre: 'objeto padre',
        getValue: function(){
            return this.valuePadre;
        }
    }
    objetoPadre.objetoHijo =  {
        valueHijo: 'objeto hijo',
        getValue: function(){
            console.log(this);
            return this.valuePadre + " - " + this.valueHijo;
        }
    }
    render(objetoPadre.getValue());
    render(objetoPadre.objetoHijo.getValue());

    /*
    expresión de objetos: patrón módulo
    basado en (function (conexto){})(contexto)
    this en
     */
    var miModulo = (function(window) {
        var value = 'módulo';
        function _getValue(){
            return value;
        }
        function _getContext(){
            return this;
        }
        // gracias a este return {} el conexto se convierte en interno
        return {
            getValue: function(){
                return _getValue();
            },
            getContext: function(){
                return _getContext();
            }
        }
    })(window);
    render(miModulo.getValue());
    // conexto pasado en la función
    console.log(miModulo.getContext());
    // conexto pasado con bind
    console.log(miModulo.getContext.bind(miModulo));
    var getContext = miModulo.getContext.bind(this);
    console.log(getContext());

    /*
    submódulos. Poca separación entre ámbitos
     */
    var miModuloPadre = (function(window) {
        var valuePadre = 'módulo padre';
        function _getValue(){
            return valuePadre;
        }
        var _miModuloHijo = (function(){
            var valueHijo = "módulo hijo";
            return {
                getValue: function(){
                    return valuePadre + " - " + valueHijo;
                }
            }
        })();
        // gracias a este return {} el conexto se convierte en interno
        return {
            getValue: function(){
                return _getValue();
            },
            miModuloHijo: _miModuloHijo
        }
    })(window);
    render(miModuloPadre.getValue());
    render(miModuloPadre.miModuloHijo.getValue());
});















