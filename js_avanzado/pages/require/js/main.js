
/*
uso simple de require: carga la clase cuando la necesita
 */
/*
requirejs(["clases/circle"], function(CircleClass) {
    var circle = CircleClass.createCircle(50);
    circle.render();
});
*/

//require para varios módulos
//y uso de paths


requirejs.config({
    baseUrl: 'js/clases',
    paths: {
        lodash: '../lib/lodash'
    },
    shim : {
        lodash : {
            exports : 'lodash'
        },
        utilidades : {
            exports : 'utilidades'
        },
        circle : {
            deps : ['lodash'],
            exports : 'circle'
        },
        app: {
            deps : ['utilidades', 'circle'],
            exports: 'app'
        }
    }
});



requirejs(['app'],
    function   (App) {
        App.init();
    }
);

