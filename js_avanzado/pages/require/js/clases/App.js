define('app', ['circle', 'utilidades'], function (CircleClass, Utilidades) {
    return{
        init: function(){
            var circle = CircleClass.createCircle(50);
            Utilidades.render(circle.render());
        }
    }
});