(function($){
    $.fn.acordeon_plugin = function(options){
        var settings = $.extend({
            title: "",
            data: [],
            useCss: false,
        }, options );

        var $container = $('<div class="acordeon"></div>');
        var $title = $('<div class="acordeon_title"></div>')
            .html('<h2>'+settings.title+'</h2>');
        $container.append($title);
/*
        settings.data.map(function(item){
            console.log(item)
        });
*/
        for(var i=0; i<settings.data.length; i++){
            var $header = $('<h2 class="acordeon_header"></h2>')
                .text(settings.data[i].nombre);
            var $b_mas = $('<button class="b_mas btn">Ampliar</button>');
            var $content = $('<div class="acordeon_content"></div>')
                .text(settings.data[i].contenido)
                //.css('height', 0)
                .append($b_mas);
            var $bloque = $('<div class="acordeon_bloq"></div>')
                .addClass('closed')
                .append($header)
                .append($content);
            if(settings.useCss){
                $container
                    .addClass('useCss');
            }else{
                $content
                    .css('height', 0);
            }
            $container.append($bloque);

            $header.on('click', function() {
                var $bloque = $(this).parent();
                var $content = $(this).parent().find('.acordeon_content');
                if($bloque.hasClass('closed')){
                    if(!settings.useCss){
                        abrir($content);
                    }else{
                        abrirCss($content);
                    }
                }else{
                    if(!settings.useCss){
                        cerrar($content);
                    }else{
                        cerrarCss($content);
                    }
                }
                $bloque.toggleClass('closed')
            });
            $b_mas.on('click', function(){
                var text = $(this)
                    .parent()
                    .clone()    //clone the element
                    .children() //select all the children
                    .remove()   //remove all the children
                    .end()  //again go back to selected element
                    .text();
                $('body').modal_plugin({text:text})
            });
        }

        this.append($container);

        return this;
    };

    function abrir(element){
        var alto = element.get(0).scrollHeight;
        console.log(alto);
        element
            .animate({
                opacity: 1,
                height: alto+'px',
            }, 1000, 'easeInOutQuart');
    }
    function cerrar(element){
        element
            .animate({
                opacity: 0,
            }, 200)
            .animate({
                height: 0,
            }, 600, 'easeInOutQuart');
    }
    /*
    function abrir_js(element, valor, callback){
        element.animate({
            height: valor,
        }, 500, function() {
            if (callback) callback();
        });
    }
    */

    function abrirCss(element){
        element.addClass('open');

    }
    function cerrarCss(element){
        element.removeClass('open');
    }
})($)