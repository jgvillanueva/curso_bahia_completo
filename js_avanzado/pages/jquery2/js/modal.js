(function($){
    $.fn.modal_plugin = function(options){
        var settings = $.extend({
            text: "",
        }, options );

        var html = '<div id="modal_window">';
        html += '<div id="modal_container">';
        html += '<div id="modal_content">';
        html += settings.text;
        html += '<button class="b_close">cerrar</button>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        var $html = $(html);
        this.append($html);

        $modal_window = this.find('#modal_window')
            .css({
                position: 'fixed',
                top:0,
                bottom:0,
                left:0,
                right:0,
                background: 'rgba(0,0,0,0.7)',
                opacity: 0
            });
        $modal_container = this.find('#modal_container')
            .css({
                width: '300px',
                margin: 'auto',
                marginTop: '200px',
                background: '#fff',
                display: 'flex',
                padding: '20px 10px',
                left: '-300px',
                right: 0,
                position: 'relative'
            });
        this.find('.b_close')
            .css({
                width: '100px',
                margin: 'auto',
                marginTop: '20px',
                display: 'block'
            });

        function close(){
            $(this).off('click', close)
            $html.remove();
        }
        this.find('.b_close').on('click', close)

        function open(){
            $modal_window
                .animate({
                    opacity: 1,
                }, { duration: 1000, queue: false } );
            $modal_container
                .animate({
                    left: '0',
                }, { duration: 1500, queue: false, easing: 'easeOutQuart'}  );
        }
        open();

        return this;
    };


})($)