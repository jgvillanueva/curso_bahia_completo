$( document ).ready(function() {
    console.log("Página cargada");
});


var globalVar = "Global: Este es el contenido de la vasriable global";

function testVariables(){
    var localVar = "Local: Este es el contenido de la vasriable local";
    console.log('globalVar', globalVar);
    console.log('localVar', localVar);

    //error: definir variable sin var
    errorLocal = "Este valor se convierte en globasl";
    console.log("This local: ", this);

    function nivel1(){
        var nivel1Var = "Local Nivel 1: Este es el contenido de la vasriable local en nivel 1";
        console.log('localVar en nivel 1', localVar);
        console.log('nivel1Var: ', nivel1Var);
    }
    //console.log('nivel1Var: ', nivel1Var);

    var internalData = {
        getData: function(){
            console.log('This desde el interior de un objeto', this);
        }
    }
    internalData.getData();

    // uso de bind:
    this.x = 10;
    var module = {
        x: 100,
        getX: function(){
            return this.x;
        }
    }

    console.log("X desde Global", this.x);
    console.log("X desde local", module.getX());

    var getX = module.getX;
    console.log("X desde Global con función local", getX());

    var boundGetX = module.getX.bind(module)
    console.log("X desde Global con función local y BIND", boundGetX());
}
testVariables();

console.log('globalVar', globalVar);
//console.log('localVar', localVar);

//error: definir variable sin var
console.log('errorLocal', errorLocal);

console.log("This global: ", this);


// funciones autoejecutables
(function (window){
    miFuncion = this;
    miFuncion.log = function(text){
        console.log(text);
    }
})(window)
miFuncion.log("Resultado del log en el autoejecutable");

/*
uso de bind
 */
var x = 2000;
var module = {
    x: 100,
    getX: function(){
        return this.x;
    }
}
console.log("sin bind:", module.getX());
var functionReference = module.getX.bind(this);
console.log("con bind:", functionReference());