;
(function($, window, document){
    $.fn.tabla_plugin = function(){
        // carga previa del contenedor para usarlo después
        this.$container = $('<ul class="ul_container"></ul>');
        // uso de binding para no perder el contexto
        this.render = _render.bind(this);
        this.reload = _reload.bind(this);
        this.creaBotonera = _creaBotonera.bind(this);

        this.creaBotonera();

        this.reload();

        return this;
    }

    function _reload(){
        var _this = this;
        $().services_plugin({
            urlBase: 'http://dev.contanimacion.com/birds/public/',
        })
            .load_data(
                'getBirds/1',
                function(data){
                    _this.render(data);
                });
    }

    function _render(data){
        console.log(data, this.$container);
        for(var i=0; i<data.length; i++){
            var $img = $('<img></img>')
                .attr('src', data[i].bird_image);
            var $text = $('<h3></h3>')
                .text(data[i].bird_name);
            var many = data[i].bird_sightings > 10 ? 1:0;
           var $li = $('<li></li>')
               .attr("data-ref", data[i].id)
               .attr("data-many", many)
               .attr("data-vistas", data[i].bird_sightings)
               .append($img)
               .append($text);

            $li.click(function(){
                console.log($(this).data('ref'));
                $(this).clone().appendTo('body').modal();
            })

            this.$container
                .append($li);
        }
        this.append(this.$container);
    }

    function _creaBotonera(){
        var _this = this;

        var $botonera = $("<div></div>");
        var b_clean = $('<button class="b_clean">Clean</button>')
            .on('click', clean);
        var b_reload = $('<button class="b_reload">Reload</button>')
            .on('click', reload);
        var b_sort = $('<button class="b_sort">Sort</button>')
            .on('click', sort);
        var b_menos = $('<button class="b_sort">Menos vistos</button>')
            .on('click', menos_vistos);

        $botonera.append(b_clean);
        $botonera.append(b_reload);
        $botonera.append(b_sort);
        $botonera.append(b_menos);
        this.prepend($botonera);

        function clean(){
            $(".b_clean",  _this.$container).off('click', clean);
            $(".b_reload",  _this.$container).off('click', reload);
            $(".b_sort",  _this.$container).off('click', sort);
            $(".b_menos",  _this.$container).off('click', menos_vistos);

            _this.$container.html("");
        }
        function reload(){
            clean();
            _this.reload();
        }
        function sort(){
            $ul = _this.$container.clone();
            clean();
            var elementos = $('li', $ul).toArray();
            elementos = quickSort(elementos);
            _this.$container.append(elementos);
        }
        function quickSort(lista){
            if(lista.length < 2){
                return lista;
            } else {
                var left = [];
                var right = [];
                var newArray = [];
                var pivot = lista.pop();
                var length = lista.length;

                for (var i = 0; i < length; i++) {
                    if ($(lista[i]).data('vistas') <= $(pivot).data('vistas')) {
                        left.push(lista[i]);
                    } else {
                        right.push(lista[i]);
                    }
                }

                return newArray.concat(quickSort(left), pivot, quickSort(right));
            }
        }
        function menos_vistos(){
            _this.$container.find('li[data-many="1"]')
                .toggleClass('hidden')
        }
    }





})($, window, document)

