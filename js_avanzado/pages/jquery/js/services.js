;
(function($, window, document){
    $.fn.services_plugin = function(options){
        // valores por defecto
        var settings = $.extend({
            urlBase: "",
            method: 'get',
            dataType: 'json'
        }, options );

        return {
            //métodos del servicio
            load_data: function(url, callback){
                $.ajax({
                    url: settings.urlBase + url,
                    dataType: settings.dataType,
                    method: settings.method,
                })
                    .done(function(data) {
                        callback(data);
                    })
                    .always(function(result) {
                        checkStatus(result);
                    });
            },
            // encadenamiento de llamadas
            load_multiple: function(url_array){
                var load = function(url, callback){
                    $.ajax({
                        url: url,
                    })
                        .done(function(data) {
                            callback(data);
                            if(index < url_array.length - 1){
                                index ++;
                                reload();
                            }else{
                                console.log("Carga completada");
                            }
                        })
                        .always(function(result) {
                            checkStatus(result);
                        });
                }
                var index = 0;
                var reload = function(){
                    load(url_array[index].url, url_array[index].callback);
                }
                reload();
            }
        }
    }

    //funciones estáticas
    $.fn.services_plugin.test = function(url){
        $.ajax({
            url: url,
        })
            .always(function(result) {
                if(result.status !== undefined && result.status !== '200'){
                    console.log("La url genera error", result.status);
                } else {
                    console.log("La url funciona");
                }
            });
    }

    //funciones privadas
    function checkStatus(result){
        switch(result.status){
            case 404:
                alert("Página no encontrada");
                break;
            case 500:
                alert("Error en el servidor");
                break;
        }
    }

})($, window, document)

