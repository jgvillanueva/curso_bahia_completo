$( document ).ready(function() {
    console.log("Página cargada");

    initTemplates();

    $context = $(".container");
    $("#data_container", $context)
        .tabla_plugin();

    $.fn.services_plugin.test('http://dev.contanimacion.com/birds/public/getBirds/1');

    $().services_plugin({})
        .load_multiple([
        {
            url: 'http://dev.contanimacion.com/birds/public/getBirds/1',
            callback: function(data){
                console.log('llega la primera llamada', data);
            }
        },
        {
            url: 'http://multimedia.uoc.edu/frontend/getproducts.php?page=1',
            callback: function(data){
                console.log('llega la segunda llamada', data);
            }
        },
    ]);
});

function initTemplates(){
    var $templates = $('<div id="templates"></div>');
    $('body').append($templates);
    $templates.load('templates/li.html');
}
