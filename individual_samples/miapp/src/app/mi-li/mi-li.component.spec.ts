import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiLiComponent } from './mi-li.component';

describe('MiLiComponent', () => {
  let component: MiLiComponent;
  let fixture: ComponentFixture<MiLiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiLiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiLiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
