import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-mi-li',
  templateUrl: './mi-li.component.html',
  styleUrls: ['./mi-li.component.css']
})
export class MiLiComponent implements OnInit {

  _texto:string = "";
  @Input()
  set texto(value){
    this._texto = value;
  }
  get texto(){
    return this._texto;
  }

  _ref:number = 0;
  @Input()
  set ref(value){
    this._ref = value;
  }
  get ref(){
    return this._ref;
  }

  constructor() { }

  ngOnInit() {
  }

}
