import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RotuloComponent } from './rotulo.component';

describe('RotuloComponent', () => {
  let component: RotuloComponent;
  let fixture: ComponentFixture<RotuloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RotuloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RotuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
