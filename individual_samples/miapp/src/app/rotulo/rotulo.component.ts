import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-rotulo',
  templateUrl: './rotulo.component.html',
  styleUrls: ['./rotulo.component.css']
})
export class RotuloComponent implements OnInit {

  @Input() descripcion: string;

  datos1:[] = ['Jorge', 'Juan', 'Pedro']
  datos2:Array<any> = [
    {id:0, nombre:'Juan'},
    {id:2, nombre:'Carlos'},
    {id:3, nombre:'Ana'},
    {id:4, nombre:'Susana'},
    {id:5, nombre:'Pedro'},
  ]

  miRotulo:string;

  textoInput:string;

  muestraComponente:boolean = false;

  constructor() {

  }

  ngOnInit() {
    this.miRotulo = "La app de Jorge";
  }

  clickHandler(){
    this.muestraComponente = !this.muestraComponente;
  }

}
