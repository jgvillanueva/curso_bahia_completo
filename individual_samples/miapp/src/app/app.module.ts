import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { RotuloComponent } from './rotulo/rotulo.component';
import { ResultadoComponent } from './resultado/resultado.component';
import { MiLiComponent } from './mi-li/mi-li.component';


@NgModule({
  declarations: [
    AppComponent,
    RotuloComponent,
    ResultadoComponent,
    MiLiComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
