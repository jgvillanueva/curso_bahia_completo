export module UsersModule{
    export class Users {
        data: Array<User>
        private _ciudad: string;

        constructor(data: Array<User>) {
            this.data = data;
        }

        getDataAge() {
            let total: number = 0;
            for (let i = 0; i < this.data.length; i++) {
                total += this.data[i].edad;
            }
            return total;
        }

        public set ciudad(valor:string){
            this._ciudad = valor;
        }

        public get ciudad():string{
            return this._ciudad;
        }
    }

    export class User {
        nombre: string;
        edad: number;
    }

}



