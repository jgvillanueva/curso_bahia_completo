import { Polygon, IPolygon } from "./Poligon";

export class Circle extends Polygon implements ICircle{
    public area: number;
    radio: number;
    static PI: number = Math.PI;

    constructor(radio:number) {
        super();
        this.area = 2*Circle.PI*radio;
    }

    public getArea():number {
        return this.area;
    }
}

export class SuperCircle extends Circle implements ICircle{

    constructor(radio:number) {
        super(radio);
        this.area = 2*SuperCircle.PI*radio;
    }

    public getArea():number {
        return 1000*this.area;
    }
}

export interface ICircle extends IPolygon{
    radio:number;
    getArea():number;
}


