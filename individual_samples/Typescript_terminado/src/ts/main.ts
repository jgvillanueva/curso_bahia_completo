import { UsersModule } from "./Users";

import { Circle, SuperCircle } from "./Circle";


import {Promise} from 'es6-promise';
import {LodashTest} from "./LodashTest";

//TIPADO

//number, boolean, string, any
//VOID, NULL, Undefined
var numero: number = 2;

function getDoble(valor: number): number {
    return valor * numero;
}

//tipado de arrays y objetos
var listaNumeros: Array<number> = [1, 2, 3];
var listaStrings: string[] = ["uno", "dos"];
const user: { "nombre", "edad" } = {
    nombre: "Paco",
    edad: 24,
}


//ÁMBITOS
//uso de let
for (var i: number = 0; i < 10; i++) {
    //console.log(i);
}
//console.log(i);
for (let x: number = 0; x < 10; x++) {
    //  console.log(x);
}
//console.log(x);

//funciones anónimas
let z = 1;
setTimeout(()=>{
    console.log(z);
}, 1000);

//const
const constante: string = "mi constante";
//constante += " es constante";

//objetos y constantes
//  console.log(user);
user.edad = 45;
//  console.log(user);


//let circe:ICircle
let circle:Circle = new Circle(3);
console.log(circle.getArea());

let usuarios: Array<UsersModule.User> = [user, {nombre: "Jorge", edad: 47}]
let users: UsersModule.Users = new UsersModule.Users(usuarios);
console.log(users.getDataAge());

users.ciudad = "León";
console.log(users.ciudad);

let promises = getData(false)
    .then((data)=>{
        console.log(data);
    })
    .catch((error)=>{
        console.log(error);
    })
function getData(resultado){
    let __this = this;
    return new Promise((resolve, reject) => {
        setTimeout(()=>{
            if(resultado){
                let datos = {txt: "Funciona"};
                resolve(datos);
            }else{
                let error = {txt: "Falla"};
                reject(error);
            }

        }, 5000)
    });
}

let _:LodashTest = new LodashTest();


import * as $ from "jquery";
$( document ).ready(function() {
    console.log("app cargada");
});