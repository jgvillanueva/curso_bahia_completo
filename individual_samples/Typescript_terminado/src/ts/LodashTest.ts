import { _ } from "lodash";
import * as $ from 'jquery';
import {Promise} from 'es6-promise';

export class LodashTest {
    timer:Timer;

    constructor(){
        this.timer = new Timer();

        this.loadData()
            .then((data)=>{
                this.procesaCSV(data);
            })
            .catch((error)=>{
                console.log(error);
            })
    }

    loadData(){
        return new Promise((resolve, reject)=>{
            $.ajax({
                type: "GET",
                url: "data/data.csv",
                dataType: "text",
                success: (data) => {
                    resolve(data);
                },
                error: (err)=>{
                    reject(err);
                }
            });
        })
    }

    procesaCSV(data:any){
        let lineas:string[], head:Array<string>, values:any
        lineas = data.split(/\r\n|\n/);
        head = _.first(lineas)
            .split(";");
        values = [];
        lineas = _.tail(lineas);
        _.forEach(lineas, (value) => {
            let _value = value.split(";");
            values.push(_value);
        });
        this.timer.start();


        let $tabla, $header, $files;
        $header = $("<ul class='table-header'></ul>");
        _.map(head, (value) => {
            $header.append("<li>"+value+"</li>");
        });
        /*_.forEach(head, (value) => {
            $header.append("<li>"+value+"</li>");
        });*/
        $files = $("<ul class='table-content'></ul>");
        _.forEach(values, (value) => {
            let row:string = "<li class='row'><ul>";
            _.forEach(value, (result) => {
                row +="<li>"+result+"</li>";
            });
            row += "</ul></li>";
            $files.append(row);
        });

        $tabla = $("<div class='table'></div>")
            .append($header)
            .append($files);

        $("#dataContainer").append($tabla);

        this.timer.stop();
    }

}

class Timer {
    private time1;
    private time2;

    constructor(){

    }

    public start(){
        this.time1 = _.now();
    }

    public stop(){
        this.time2 = _.now();
        console.log(this.time2 - this.time1);
    }
}
