

export class Polygon implements IPolygon{
    public area: number;

}

export interface IPolygon{
    area:number;
    radio?: number;

}
