var gulp = require('gulp')
    sass = require('gulp-sass')
    sourcemaps = require('gulp-sourcemaps')
    del = require('del')
    autoprefixer = require('gulp-autoprefixer')
    concat = require('gulp-concat')
    browserSync = require('browser-sync')
    plumber      = require('gulp-plumber')
    uglify       = require('gulp-uglify')
    jshint       = require('gulp-jshint')
    stylish      = require('jshint-stylish')
    uglifycss    = require('gulp-uglifycss')
    imagemin     = require('gulp-imagemin')
    browserify     = require('browserify')
    source = require('vinyl-source-stream')
    ts = require('gulp-typescript');

var srcPaths = {
    copyhtml: './src/*.html',
    templates: './src/templates/*.html',
    vendor: './src/vendor/**/*.js',
    data: './src/data/*.*',
    sass: './src/scss/**/*.scss',
    ts: './src/ts/**/*.ts',
    js: './src/js/**/*.js',
    css: './src/css/**/*.css',
    images: './src/img/**/*.*',
}
var distPaths = {
    copyhtml: './dist',
    templates: './dist/templates',
    vendor: './dist/vendor',
    data: './dist/data',
    sass: './dist/css',
    js: './dist/js',
    css: './dist/css',
    images: './dist/img',
}

gulp.task('clean', function (cb) {
    del(distPaths.sass, cb);
    cb();
});

gulp.task('html', function() {
    return gulp.src(srcPaths.copyhtml)
        .pipe(gulp.dest(distPaths.copyhtml))
        .pipe(browserSync.stream());
});
gulp.task('templates', function() {
    return gulp.src(srcPaths.templates)
        .pipe(gulp.dest(distPaths.templates))
        .pipe(browserSync.stream());
});// Copia de los cambios en las librerías vendor al directorio dist.
gulp.task('vendor', function() {
    return gulp.src([srcPaths.vendor])
        .pipe(gulp.dest(distPaths.vendor))
        .pipe(browserSync.stream());
});
// Copia de los cambios en el archivo de configuración de contactos inicial al directorio dist.
gulp.task('data', function() {
    return gulp.src([srcPaths.data])
        .pipe(gulp.dest(distPaths.data))
        .pipe(browserSync.stream());
});
gulp.task('copy', gulp.series('html', 'templates', 'vendor', 'data'));


gulp.task('reload', function () {
    return gulp.src('./')
        .pipe(browserSync.reload({stream:true}));
});

/*
IMÁGENES
 */
// Procesamiento de imágenes para comprimir / optimizar las mismas.
gulp.task('imagemin', function() {
    return gulp.src([srcPaths.images])
        .pipe(imagemin({
            progressive: true,
            interlaced: true,
        }))
        .pipe(gulp.dest(distPaths.images))
        .pipe(browserSync.stream());
});



gulp.task('css', function() {
    return gulp.src(srcPaths.css)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('styles.css'))
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(sourcemaps.write('maps'))
        .pipe(plumber.stop())
        .pipe(gulp.dest(distPaths.css))
        .pipe(browserSync.stream());
});


gulp.task('sass', function () {
    return gulp.src(srcPaths.sass)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(concat('sass_styles.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(distPaths.sass))
        .pipe(browserSync.reload({stream:true}));
});

/*
TS
 */
var tsProject = ts.createProject('tsconfig.json');
// para externalizar en un mismo lugar las opciones de compilación del typescript
gulp.task('ts', function () {
    var tsResult = tsProject.src()
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(tsProject());
    return tsResult.js
        .pipe(plumber.stop())
        .pipe(gulp.dest("dist/temp"))
});
gulp.task('browserify', function() {
    return browserify({entries: './dist/temp/main.js'})
        .bundle()
        .pipe(source('allTS.min.js'))
        .pipe(gulp.dest(distPaths.js));
});
gulp.task('cleanJS', function(cb) {
    return del(['dist/temp'], cb);
});
gulp.task('js', function(cb) {
    return gulp.src([distPaths.js + '/allTS.min.js'])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(plumber.stop())
        .pipe(gulp.dest(distPaths.js))
        .pipe(browserSync.stream());
});
gulp.task('compileTS', gulp.series('ts', 'browserify', 'cleanJS', 'js'));



/*
js
 */
// Procesamiento de ficheros JS mediante JSHint para detección de errores.
gulp.task('lint', function() {
    return gulp.src(srcPaths.js)
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});
// Procesamiento de ficheros JS para la generación de un fichero
// final único y minificado.
// Los sourcemaps se generan en una carpeta independiente maps
gulp.task('js_prepare', function() {
    return gulp.src(srcPaths.js)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('all.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(plumber.stop())
        .pipe(gulp.dest(distPaths.js))
        .pipe(browserSync.stream());
});
gulp.task('js', gulp.series('lint', 'js_prepare'));


gulp.task('watch', function () {
    gulp.watch(srcPaths.sass, gulp.series('sass'));
    gulp.watch(srcPaths.ts, gulp.series('compileTS'));
    gulp.watch(srcPaths.js, gulp.series('js'));
    gulp.watch(srcPaths.css, gulp.series('css'));
    gulp.watch("./*.html", gulp.series('reload'));
});
gulp.task('serve', function(cb) {
    browserSync({
        server: {
            baseDir: "./dist"
        },
        port: 4000,
        notify: true,
        open: true
    }, cb);
});

gulp.task('all', gulp.parallel('copy', 'sass', 'compileTS', 'js', 'css', 'imagemin'));
gulp.task('build', gulp.series('clean', 'all'));

gulp.task('default', gulp.series('build', 'serve', 'watch'));