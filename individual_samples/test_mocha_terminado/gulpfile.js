var gulp = require('gulp');
var mocha = require('gulp-mocha');
var gutil = require('gulp-util');
var plumber = require('gulp-plumber');

gulp.task('mocha', function() {
    return gulp.src(['./tests/**/*.ts'])
        .pipe(plumber())
        .pipe(mocha({
            reporter: 'list',//'nyan'
            require: ['ts-node/register']
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest(['./tests/**/*.ts']))
});

gulp.task('watch', function(){
    gulp.watch(['./src/*.ts', './tests/**/*.ts'], gulp.series('mocha'));
});


gulp.task('default', gulp.series('mocha', 'watch'));