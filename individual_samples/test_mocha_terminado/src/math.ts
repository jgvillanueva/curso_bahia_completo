export function multiply(x, y) {
    return x*y;
}

export function factorial(n) {
    if(n<=0){
        return 'El parámetro ha de ser mayor que 0'
    }
    var total = 1;
    for (let i=1; i<=n; i++) {
        total = total * i;
    }
    return total;
}
