import hello from '../src/hello';
import { expect } from 'chai';
// if you used the '@types/mocha' method to install mocha type definitions, uncomment the following line
// import 'mocha';

describe('Función hello', () => {
    it('tiene que devolver Hola mundo!', () => {
        const result = hello();
        expect(result).to.equal('Hola mundo!');
    });
});