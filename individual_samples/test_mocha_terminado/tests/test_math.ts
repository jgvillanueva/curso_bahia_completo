import {multiply, factorial} from '../src/math';
import { expect } from 'chai';

describe('Test de multiplicación', () => {
    it('multiplicación por 0', () => {
        const result = multiply(5, 0);
        expect(result).to.equal(0);
    });
    it('multiplicación ha de devolver un nnúmero', () => {
        const result = multiply(100, 100);
        expect(result).to.be.a('number');
    });
});

describe('Test de factorial', () => {
    it('factorial de 8', () => {
        const result = factorial(8);
        expect(result).to.equal(40320);
    });
    it('El factorial en números negativos ha de devolver un string', () => {
        const result = factorial(-7);
        expect(result).to.be.a('string');
    });
});


/*
import {multiply, factorial} from '../src/math';
import { assert } from 'chai';

describe('Test de multiplicación', () => {
    it('multiplicación por 0', () => {
        const result = multiply(5, 0);
        assert.equal(result, 0);
    });
    it('multiplicación ha de devolver un nnúmero', () => {
        const result = multiply(100, 100);
        assert.isNumber(result);
    });
});

describe('Test de factorial', () => {
    it('factorial de 8', () => {
        const result = factorial(8);
        assert.equal(result, 40320);
    });
    it('El factorial en números negativos ha de devolver un string', () => {
        const result = factorial(-7);
        assert.isString(result);
    });
});

 */