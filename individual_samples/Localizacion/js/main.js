$( document ).ready(function() {
    console.log("Página cargada");

    $.i18n.debug = true;
    $.i18n().locale = 'en';
    var update_texts = function() {
        $('body').i18n();
    };

    /*
    $.i18n().load({
        en: {
            'text-p1': 'Hello World',
            'text-p2': 'Welcome'
        },
        es: {
            'text-p1': 'Hola mundo',
            'text-p2': 'Hola'
        }
    });
    */
    $.i18n().load( {
        'en': 'i18n/en.json',
        'es': 'i18n/es.json',
    } )
        .done(function(){
            console.log('llega');
            console.log($.i18n().localize( 'text-p1' ));
            update_texts();
        })


    setTimeout(function(){
        $.i18n().locale = 'es';
        update_texts();
    }, 5000)



});
