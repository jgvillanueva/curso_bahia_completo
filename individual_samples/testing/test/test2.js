var assert    = require("chai").assert;

var main = require("../js/main");

describe("Usando CHAI para tests con main.js: ", function() {
    describe("Control resultado suma: ", function() {
        it("Comprobando que el resultado sea una suma", function() {
            result   = main.suma(1,2);
            assert.equal(result, 3);
        });

        it("Control tipo de resultado devuelto por suma: ", function() {
            result   = main.suma(1,2);
            assert.typeOf(result, "number");
        });
    });

    describe("Control resultado lista: ", function() {
        it("Comprobando que el resultado tenga 2 elementos", function() {
            result   = main.lista(1,2);
            assert.lengthOf(result, 2);
        });

        it("Control tipo de resultado devuelto por lista: ", function() {
            result   = main.lista(1,2);
            assert.typeOf(result, "array");
        });
    });

    describe("Control resultado objeto: ", function() {
        it("Comprobando que el resultado tenga 2 elementos", function() {
            result   = main.objeto(1,2);
            assert.deepEqual(result, {valor1:1, valor2:2});
        });

        it("Control tipo de resultado devuelto por lista: ", function() {
            result   = main.objeto(1,2);
            assert.typeOf(result, "object");
        });
    });
});


