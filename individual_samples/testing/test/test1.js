var assert = require('assert');
describe('Prueba simple', function() {
    describe('Comprueba si indexOf() funciona', function() {
        it('debería devolver el valor -1 si el valor no está presente', function(){
            assert.equal(-1, [1,2,3].indexOf(4));
        });
    });
});