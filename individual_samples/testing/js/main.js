

module.exports.suma = function(valor1, valor2){
    return valor1 + valor2;
};

module.exports.lista = function(valor1, valor2){
    return [valor1, valor2];
};

module.exports.objeto = function(valor1, valor2){
    return {valor1: valor1, valor2: valor2};
};

