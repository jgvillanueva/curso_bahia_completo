//node server.js
//npm install -g nodemon

//NGINX
//en mac: brew services start nginx
//en windows descarga de archivos desde su web y ejecución de nginx start
//pararlo con brew services stop nginx

//cambio de ruta: config/nginx.conf en location (cuidado que hay dos)
//en mac /usr/local/etc/nginx
//creación del proxi en el apartado location, debajo del resto:
/*
location / {
            root   /Jorge/Curso_JS_Avanzado/4_Require_modules;
            index  index.html index.htm;

            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-NginX-Proxy true;

            proxy_pass http://127.0.0.1:1337/;
            proxy_redirect off;
        }
 */


/*
servidor básico
 */

const http = require('http');
const fs = require('fs');
const rutaBase = "./templates/";

http.createServer(function (req, res) {
    //res.writeHead(200, {'Content-Type': 'text/plain'});

    //res.write("Hola Mundo");
    var template, error = 0;
    switch (req.url){
        case "/":
            template = "index.html";
            break;
        case "/test":
            template = "test.html";
            break;
        default:
            template = "404.html";
            error = 1;
            break;
    }

    fs.readFile(rutaBase + template, function(error, data){
        if(error = 0){
            res.writeHead(200, { 'Content-Type': 'text/html' });
        }else{
            res.writeHead(404, { 'Content-Type': 'text/html' });
        }
        res.write(data);
        res.end();
    });
}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');



